#include "benchmark.h"
#include "prototypes.h"


int main( int argc, char *argv[] )
{
    if( sizeof( char ) != 1 ) return 1;

    SUCCESS_OR_DIE( gaspi_proc_init( GASPI_BLOCK ) );

    gaspi_rank_t rank, total_nmbr_ranks;
    SUCCESS_OR_DIE( gaspi_proc_rank( &rank ) );
    SUCCESS_OR_DIE( gaspi_proc_num( &total_nmbr_ranks ) );

    /*
    if( 1 >= total_nmbr_ranks )
    {
        if( !rank ) fprintf( stderr, "At least two prcs needed\n" );
        SUCCESS_OR_DIE( gaspi_proc_term( GASPI_BLOCK ) );
        return 1;
    }
    */
    if( total_nmbr_ranks != 2 )
    {
        if( !rank ) fprintf( stderr, "Launch w/ exactly two prcs\n" );
        SUCCESS_OR_DIE( gaspi_proc_term( GASPI_BLOCK ) );
        return 1;
    }


#ifdef DEBUG
    gaspi_number_t queue_depth_max;
    SUCCESS_OR_DIE( gaspi_queue_size_max( &queue_depth_max ) );
    if( !rank ) fprintf( stderr, "DEBUG: Max queue depth: %d\n", queue_depth_max );
#endif

    const unsigned int nmbr_dlngths = 14u;
    benchData_t *data = (benchData_t *)malloc( sizeof( benchData_t ) + nmbr_dlngths*sizeof( benchIter_t ) );
    prepare_data( 1u, sizeof( double ), nmbr_dlngths, NMBR_SAMPLES_DEFAULT, false, WRTE_READ_RND_DATA_DEFAULT, data );


    if( !rank ) fprintf( stderr, "notfy_ud\n" );
    run_notfy_ud( rank, data );
    if( !rank ) fprintf( stderr, "notfy_bd\n" );
    run_notfy_bd( rank, data );
    if( !rank ) fprintf( stderr, "wrte_ud\n" );
    run_wrte_ud( rank, data );
    if( !rank ) fprintf( stderr, "wrte_bd\n" );
    run_wrte_bd( rank, data );
    if( !rank ) fprintf( stderr, "wrte_notfy_ud\n" );
    run_wrte_notfy_ud( rank, data );
    if( !rank ) fprintf( stderr, "wrte_notfy_bd\n" );
    run_wrte_notfy_bd( rank, data );
    if( !rank ) fprintf( stderr, "read_ud\n" );
    run_read_ud( rank, data );
    if( !rank ) fprintf( stderr, "read_bd\n" );
    run_read_bd( rank, data );

    data->threads_each = 2u;
    if( !rank ) fprintf( stderr, "wrte_mt_ud\n" );
    run_wrte_mt_ud( rank, data );
    if( !rank ) fprintf( stderr, "read_mt_ud\n" );
    run_read_mt_ud( rank, data );
    data->threads_each = 4u;
    if( !rank ) fprintf( stderr, "wrte_mt_ud\n" );
    run_wrte_mt_ud( rank, data );
    if( !rank ) fprintf( stderr, "read_mt_ud\n" );
    run_read_mt_ud( rank, data );
    data->threads_each = 8u;
    if( !rank ) fprintf( stderr, "wrte_mt_ud\n" );
    run_wrte_mt_ud( rank, data );
    if( !rank ) fprintf( stderr, "read_mt_ud\n" );
    run_read_mt_ud( rank, data );
    data->threads_each = 16u;
    if( !rank ) fprintf( stderr, "wrte_mt_ud\n" );
    run_wrte_mt_ud( rank, data );
    if( !rank ) fprintf( stderr, "read_mt_ud\n" );
    run_read_mt_ud( rank, data );
    data->threads_each = 1u;


    data->opt.alternative_mode = true;
    data->nmbr_samples = 100u;

    if( !rank ) fprintf( stderr, "notfy_ud\n" );
    run_notfy_ud( rank, data );
    if( !rank ) fprintf( stderr, "notfy_bd\n" );
    run_notfy_bd( rank, data );
    if( !rank ) fprintf( stderr, "wrte_ud\n" );
    run_wrte_ud( rank, data );
    if( !rank ) fprintf( stderr, "wrte_bd\n" );
    run_wrte_bd( rank, data );
    if( !rank ) fprintf( stderr, "wrte_notfy_ud\n" );
    run_wrte_notfy_ud( rank, data );
    if( !rank ) fprintf( stderr, "wrte_notfy_bd\n" );
    run_wrte_notfy_bd( rank, data );
    if( !rank ) fprintf( stderr, "read_ud\n" );
    run_read_ud( rank, data );
    if( !rank ) fprintf( stderr, "read_bd\n" );
    run_read_bd( rank, data );

    data->threads_each = 2u;
    if( !rank ) fprintf( stderr, "wrte_mt_ud\n" );
    run_wrte_mt_ud( rank, data );
    if( !rank ) fprintf( stderr, "read_mt_ud\n" );
    run_read_mt_ud( rank, data );
    data->threads_each = 4u;
    if( !rank ) fprintf( stderr, "wrte_mt_ud\n" );
    run_wrte_mt_ud( rank, data );
    if( !rank ) fprintf( stderr, "read_mt_ud\n" );
    run_read_mt_ud( rank, data );
    data->threads_each = 8u;
    if( !rank ) fprintf( stderr, "wrte_mt_ud\n" );
    run_wrte_mt_ud( rank, data );
    if( !rank ) fprintf( stderr, "read_mt_ud\n" );
    run_read_mt_ud( rank, data );
    data->threads_each = 16u;
    if( !rank ) fprintf( stderr, "wrte_mt_ud\n" );
    run_wrte_mt_ud( rank, data );
    if( !rank ) fprintf( stderr, "read_mt_ud\n" );
    run_read_mt_ud( rank, data );
    //data->threads_each = 1u


    SUCCESS_OR_DIE( gaspi_proc_term( GASPI_BLOCK ) );
    return 0;
}