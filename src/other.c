#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <stdio.h>
#define NPTR_TEST(ptr) \
{ \
    if( (ptr) == NULL ) \
    { \
        fprintf( stderr, "Mem alloc didn't work\n" ); \
        exit( 1 ); \
    } \
}


bool power_of( const unsigned int uval, const unsigned int base )
{
    unsigned int x = 1u;
    while( uval >= x )
    {
        if( x == uval )
            return true;

        x *= base;
    }
    return false;
}

bool multple_of( const unsigned int uval, const unsigned int a )
{
    unsigned int b = 0u;
    while( uval >= b )
    {
        if( b == uval )
            return true;

        b += a;
    }
    return false;
}

unsigned int calc_pow( const unsigned int base, const unsigned int exponent )
{
    return (unsigned int)pow( base, exponent );
}

unsigned int calc_log2( const unsigned a )
{
    return (unsigned int)log2( a );
}

unsigned int calc_mod( const unsigned int a, const unsigned int b )
{
    return (unsigned int)a%b;
}

int compare_dvalues( void const * const a, void const * const b )
{
    double c = *(double const * const)a;
    double d = *(double const * const)b;

    return ( c > d ) - ( d > c );
}

double calc_median( double* const samples, const unsigned int nmbr_smpls, const bool preserve )
{
    if( nmbr_smpls == 0u )
        exit( 1 );
    else if( nmbr_smpls == 1u )
        return *samples;
    
    double *dptr;
    if( preserve )
    {
        dptr = (double *)malloc( nmbr_smpls*sizeof( double ) );
        NPTR_TEST(dptr);
        memcpy( dptr, samples, nmbr_smpls*sizeof( double ) );
    }
    else
        dptr = samples;

    qsort( dptr, nmbr_smpls, sizeof( double ), compare_dvalues );

    double median = 0.;
    if( nmbr_smpls%2 )
        median += dptr[ ( nmbr_smpls-1 ) / 2 ];
    else
        median += ( dptr[ nmbr_smpls/2 - 1 ] + dptr[ nmbr_smpls/2 ] ) / 2;

    if( preserve )
        free( dptr );

    return median;
}

double calc_average( double const * const samples, const unsigned int nmbr_smpls )
{
    double average = 0.;
    for( unsigned int k = 0u; k < nmbr_smpls; ++k )
        average += *( samples+k );
    average /= (double)nmbr_smpls;

    return average;
}

double get_max( double const * const samples, const unsigned int nmbr_smpls )
{
    double max = samples[0];
    for( unsigned int k = 1u; k < nmbr_smpls; ++k )
        max = max>=samples[k] ? max : samples[k];
    
    return max;
}


#include <sys/time.h>
/*
unsigned int get_t_elapsed( struct timeval const * const begin, struct timeval const * const end )
{
    unsigned int t_elapsed = end->tv_usec - begin->tv_usec + 1000000u*( end->tv_sec - begin->tv_sec );
    return t_elapsed;
}
*/
double now(  )
{
    struct timeval exactly_now;
    if( gettimeofday( &exactly_now, NULL ) == -1 )
        exit( 1 );
    
    return (double)exactly_now.tv_sec + 1.e-6*(double)exactly_now.tv_usec;
}


#define PMASK_32 0xb4bcd35c
#define PMASK_31 0x7a5bc2e3

unsigned int lfsr32 = 0x89abcdef;
unsigned int lfsr31 = 0x12345678;

void init_lfsrs(  )
{
    /* Seeding the linear-feedback shift registers */
    lfsr32 = 0x89abcdef;
    lfsr31 = 0x12345678;
}

unsigned int shift_lfsr( unsigned int* const lfsr, const unsigned int polynomial_mask )
{
    bool feedback = *lfsr & 1;
    *lfsr >>= 1;
    if( feedback )
        *lfsr ^= polynomial_mask;
    return *lfsr;
}

unsigned int get_rnd_uval(  )
{
    /* This random number generator shifts the 32-bit LFSR twice before XORing 
    it with the 31-bit LFSR. The bottom 16 bits are used for the random number */
    shift_lfsr( &lfsr32, PMASK_32 );
    /* Returns a random number between 0 and 65535 */
    return ( shift_lfsr( &lfsr32, PMASK_32 ) ^ shift_lfsr( &lfsr31, PMASK_31 ) ) & 0xffff;
}

double get_rnd_dval(  )
{
    shift_lfsr( &lfsr32, PMASK_32 );
    /* 1 / 65535.0 = 1.525902189....e-5 */
    return ( ( shift_lfsr( &lfsr32, PMASK_32 ) ^ shift_lfsr( &lfsr31, PMASK_31 ) ) & 0xffff ) * 1.52590219e-5;
}