#include "benchmark.h"
#include "prototypes.h"

//#include <omp.h>


void prepare_data( const unsigned int threads_each, const gaspi_size_t dlngth_start, const unsigned int nmbr_dlngths, const unsigned int nmbr_samples, const bool alternative_mode, const bool wrte_read_rnd_data, benchData_t* const data )
{
    gaspi_rank_t rank, total_nmbr_ranks;
    SUCCESS_OR_DIE( gaspi_proc_rank( &rank ) );
    SUCCESS_OR_DIE( gaspi_proc_num( &total_nmbr_ranks ) );
    data->nmbr_procs = total_nmbr_ranks;

    data->threads_each = 0u==threads_each ? 1u : threads_each;
    data->dlngth_start = DATA_LENGTH_BASE>=dlngth_start ? DATA_LENGTH_BASE : dlngth_start;
    data->nmbr_dlngths = 0u==nmbr_dlngths ? 1u : nmbr_dlngths;
    data->nmbr_samples = 0u==nmbr_samples ? NMBR_SAMPLES_DEFAULT : nmbr_samples;
    data->opt.alternative_mode = alternative_mode;
    data->opt.wrte_read_rnd_data = wrte_read_rnd_data;

    benchIter_t *result;
    if( power_of( data->dlngth_start, 2u ) )
    {
        for( unsigned int k = 0u; k < data->nmbr_dlngths; ++k )
        {
            result = &data->results[ k ];
            result->d_lngth = calc_pow( 2u, k )*data->dlngth_start;
            result->t = 0.;
            result->t_avg = 0.;
        }

        return;
    }
    else if( multple_of( data->dlngth_start, DATA_LENGTH_BASE ) )
    {
        for( unsigned int k = 0u; k < data->nmbr_dlngths; ++k )
        {
            result = &data->results[ k ];
            result->d_lngth = calc_pow( 10u, k )*data->dlngth_start;
            result->t = 0.;
            result->t_avg = 0.;
        }

        return;
    }
}

void reset_results( benchData_t* const data )
{
    benchIter_t *result;
    for( unsigned int k = 0u; k < data->nmbr_dlngths; ++k )
    {
        result = &data->results[ k ];
        result->t = 0.;
        result->t_avg = 0.;
    }
}


//static gaspi_queue_id_t my_queue = 0
//#pragma omp threadprivate(my_queue)

void notify_and_wait( const gaspi_segment_id_t segment_id_remote, const gaspi_rank_t rank, const gaspi_notification_id_t notification_id, const gaspi_notification_t notification_value, const gaspi_queue_id_t queue )
{
    gaspi_return_t r;
    //while( ( r = gaspi_notify( segment_id_remote, rank, notification_id, notification_value, queue, GASPI_BLOCK ) ) == GASPI_QUEUE_FULL )
    while( ( r = gaspi_notify( segment_id_remote, rank, notification_id, notification_value, queue, GASPI_BLOCK ) ) != GASPI_SUCCESS )
        SUCCESS_OR_DIE( gaspi_wait( queue, GASPI_BLOCK ) );
    
    ASSERT( r == GASPI_SUCCESS );
}

/*
void notify_and_cycle( const gaspi_segment_id_t segment_id_remote, const gaspi_rank_t rank, const gaspi_notification_id_t notification_id, const gaspi_notification_t notification_value )
{
    gaspi_number_t queue_num;
    SUCCESS_OR_DIE( gaspi_queue_num( &queue_num ) );

    gaspi_return_t r;
    //while( ( r = gaspi_notify( segment_id_remote, rank, notification_id, notification_value, my_queue, GASPI_BLOCK ) ) == GASPI_QUEUE_FULL )
    while( ( r = gaspi_notify( segment_id_remote, rank, notification_id, notification_value, my_queue, GASPI_BLOCK ) ) != GASPI_SUCCESS )
    {
        my_queue = ( my_queue + 1 )%queue_num;
        SUCCESS_OR_DIE( gaspi_wait( my_queue, GASPI_BLOCK ) );
    }
    ASSERT( r == GASPI_SUCCESS );
}
*/

void write_and_wait( const gaspi_segment_id_t segment_id_local, const gaspi_offset_t offset_local, const gaspi_rank_t rank, const gaspi_segment_id_t segment_id_remote, const gaspi_offset_t offset_remote, const gaspi_size_t size, const gaspi_queue_id_t queue )
{
    gaspi_return_t r;
    //while( ( r = gaspi_write( segment_id_local, offset_local, rank, segment_id_remote, offset_remote, size, queue, GASPI_BLOCK ) ) == GASPI_QUEUE_FULL )
    while( ( r = gaspi_write( segment_id_local, offset_local, rank, segment_id_remote, offset_remote, size, queue, GASPI_BLOCK ) ) != GASPI_SUCCESS )
        SUCCESS_OR_DIE( gaspi_wait( queue, GASPI_BLOCK ) );
    
    ASSERT( r == GASPI_SUCCESS );
}

void write_notify_and_wait( const gaspi_segment_id_t segment_id_local, const gaspi_offset_t offset_local, const gaspi_rank_t rank, const gaspi_segment_id_t segment_id_remote, const gaspi_offset_t offset_remote, const gaspi_size_t size, const gaspi_notification_id_t notification_id, const gaspi_notification_t notification_value, const gaspi_queue_id_t queue )
{
    gaspi_return_t r;
    //while( ( r = gaspi_write_notify( segment_id_local, offset_local, rank, segment_id_remote, offset_remote, size, notification_id, notification_value, queue, GASPI_BLOCK ) ) == GASPI_QUEUE_FULL )
    while( ( r = gaspi_write_notify( segment_id_local, offset_local, rank, segment_id_remote, offset_remote, size, notification_id, notification_value, queue, GASPI_BLOCK ) ) != GASPI_SUCCESS )
        SUCCESS_OR_DIE( gaspi_wait( queue, GASPI_BLOCK ) );
    
    ASSERT( r == GASPI_SUCCESS );
}

/*
void write_notify_and_cycle( const gaspi_segment_id_t segment_id_local, const gaspi_offset_t offset_local, const gaspi_rank_t rank, const gaspi_segment_id_t segment_id_remote, const gaspi_offset_t offset_remote, const gaspi_size_t size, const gaspi_notification_id_t notification_id, const gaspi_notification_t notification_value )
{
    gaspi_number_t queue_num;
    SUCCESS_OR_DIE( gaspi_queue_num( &queue_num ) );

    gaspi_return_t r;
    //while( ( r = gaspi_write_notify( segment_id_local, offset_local, rank, segment_id_remote, offset_remote, size, notification_id, notification_value, my_queue, GASPI_BLOCK ) ) == GASPI_QUEUE_FULL )
    while( ( r = gaspi_write_notify( segment_id_local, offset_local, rank, segment_id_remote, offset_remote, size, notification_id, notification_value, my_queue, GASPI_BLOCK ) ) != GASPI_SUCCESS )
    {
        my_queue = ( my_queue + 1 )%queue_num;
        SUCCESS_OR_DIE( gaspi_wait( my_queue, GASPI_BLOCK ) );
    }
    ASSERT( r == GASPI_SUCCESS );
}
*/

void read_and_wait( const gaspi_segment_id_t segment_id_local, const gaspi_offset_t offset_local, const gaspi_rank_t rank, const gaspi_segment_id_t segment_id_remote, const gaspi_offset_t offset_remote, const gaspi_size_t size, const gaspi_queue_id_t queue )
{
    gaspi_return_t r;
    //while( ( r = gaspi_read( segment_id_local, offset_local, rank, segment_id_remote, offset_remote, size, queue, GASPI_BLOCK ) ) == GASPI_QUEUE_FULL )
    while( ( r = gaspi_read( segment_id_local, offset_local, rank, segment_id_remote, offset_remote, size, queue, GASPI_BLOCK ) ) != GASPI_SUCCESS )
        SUCCESS_OR_DIE( gaspi_wait( queue, GASPI_BLOCK ) );
    
    ASSERT( r == GASPI_SUCCESS );
}

void wait_or_die( const gaspi_segment_id_t segment_id, const gaspi_notification_id_t notification_id, const gaspi_notification_t expected )
{
    gaspi_notification_id_t id;
    SUCCESS_OR_DIE( gaspi_notify_waitsome( segment_id, notification_id, 1, &id, GASPI_BLOCK ) );
    ASSERT( id == notification_id );

    gaspi_notification_t value;
    SUCCESS_OR_DIE( gaspi_notify_reset( segment_id, id, &value ) );
    ASSERT( value == expected );
}

void wait_for_flush_queue( const gaspi_queue_id_t queue )
{
    SUCCESS_OR_DIE( gaspi_wait( queue, GASPI_BLOCK ) );
}

/*
void wait_for_flush_queues(  )
{
    gaspi_number_t queue_num;
    SUCCESS_OR_DIE( gaspi_queue_num( &queue_num ) );

    gaspi_queue_id_t queue = 0;

    do
    {
        SUCCESS_OR_DIE( gaspi_wait( queue, GASPI_BLOCK ) );
        ++queue;
    } while( queue_num >= queue );
}
*/

/*
void wait_if_queue_full( const gaspi_queue_id_t queue, const gaspi_number_t request_size )
{
    gaspi_number_t queue_size, queue_size_max;
    SUCCESS_OR_DIE( gaspi_queue_size( queue, &queue_size ) );
    SUCCESS_OR_DIE( gaspi_queue_size_max( &queue_size_max ) );

    if( queue_size+request_size > queue_size_max )
        SUCCESS_OR_DIE( gaspi_wait( queue, GASPI_BLOCK ) );
}
*/

void warn_if_queue_full( const gaspi_queue_id_t queue, const gaspi_number_t request_size )
{
    gaspi_number_t queue_size, queue_size_max;
    SUCCESS_OR_DIE( gaspi_queue_size( queue, &queue_size ) );
    SUCCESS_OR_DIE( gaspi_queue_size_max( &queue_size_max ) );

    if( queue_size+request_size > queue_size_max )
        fprintf( stderr, "WARN: Queue %d may overflow\n", queue );
}

void delete_segment_after_barr( const gaspi_segment_id_t segment_id, const gaspi_group_t group )
{
    SUCCESS_OR_DIE( gaspi_barrier( group, GASPI_BLOCK ) );
    SUCCESS_OR_DIE( gaspi_segment_delete( segment_id ) );
}