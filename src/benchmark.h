#ifndef BENCHMARK_H
#define BENCHMARK_H

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include <stdio.h>
#define NPTR_TEST(ptr) \
{ \
    if( (ptr) == NULL ) \
    { \
        fprintf( stderr, "Mem alloc has not worked\n" ); \
        exit( 1 ); \
    } \
}

#include <GASPI.h>
#define SUCCESS_OR_DIE(f...) \
{ \
    const gaspi_return_t r = f; \
    if( r != GASPI_SUCCESS ) \
    { \
        /*gaspi_string_t s; \
        gaspi_error_message( r, &s ); \
        fprintf( stderr, "Error: '%s' [%s:%d]: %s\n", #f, __FILE__, __LINE__, s ); \*/ \
        fprintf( stderr, "Error: '%s' [%s:%d]: %d\n", #f, __FILE__, __LINE__, r ); \
        exit( 1 ); \
    } \
}
#define ASSERT(x...) \
{ \
    if( !(x) ) \
    { \
        fprintf( stderr, "Error: '%s' [%s:%d]\n", #x, __FILE__, __LINE__ ); \
        exit( 1 ); \
    } \
}
#define RIGHT(proc,nmbr_procs) (((proc)+1)%nmbr_procs)
#define LEFT(proc,nmbr_procs) (((proc)-1)%nmbr_procs)


#define DATA_LENGTH_BASE sizeof( double )

typedef struct benchmarkIter
{
    gaspi_size_t d_lngth;
    double t;
    double t_avg;
}
benchIter_t;

typedef struct benchmarkOpt
{
    bool alternative_mode;
    bool wrte_read_rnd_data;
}
benchOpt_t;

#define NMBR_SAMPLES_DEFAULT 1000u
#define WRTE_READ_RND_DATA_DEFAULT true

typedef struct benchmarkData
{
    unsigned int nmbr_procs;
    unsigned int threads_each;
    gaspi_size_t dlngth_start;
    unsigned int nmbr_dlngths;
    unsigned int nmbr_samples;
    benchOpt_t opt;
    benchIter_t results[];
}
benchData_t;


#endif