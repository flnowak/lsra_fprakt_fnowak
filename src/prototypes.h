#ifndef PROTOTYPES_H
#define PROTOTYPES_H

#include "benchmark.h"


void notfy_ud( const gaspi_rank_t rank, const unsigned int total_nmbr_ranks, const unsigned int nmbr_samples, benchOpt_t const * const opt, benchIter_t* const result );
void run_notfy_ud( const gaspi_rank_t rank, benchData_t* const data );

void notfy_bd( const gaspi_rank_t rank, const unsigned int total_nmbr_ranks, const unsigned int nmbr_samples, benchOpt_t const * const opt, benchIter_t* const result );
void run_notfy_bd( const gaspi_rank_t rank, benchData_t* const data );

void wrte_ud( const gaspi_rank_t rank, const unsigned int total_nmbr_ranks, const unsigned int nmbr_samples, benchOpt_t const * const opt, benchIter_t* const result );
void run_wrte_ud( const gaspi_rank_t rank, benchData_t* const data );

void wrte_bd( const gaspi_rank_t rank, const unsigned int total_nmbr_ranks, const unsigned int nmbr_samples, benchOpt_t const * const opt, benchIter_t* const result );
void run_wrte_bd( const gaspi_rank_t rank, benchData_t* const data );

void wrte_notfy_ud( const gaspi_rank_t rank, const unsigned int total_nmbr_ranks, const unsigned int nmbr_samples, benchOpt_t const * const opt, benchIter_t* const result );
void run_wrte_notfy_ud( const gaspi_rank_t rank, benchData_t* const data );

void wrte_notfy_bd( const gaspi_rank_t rank, const unsigned int total_nmbr_ranks, const unsigned int nmbr_samples, benchOpt_t const * const opt, benchIter_t* const result );
void run_wrte_notfy_bd( const gaspi_rank_t rank, benchData_t* const data );

void read_ud( const gaspi_rank_t rank, const unsigned int total_nmbr_ranks, const unsigned int nmbr_samples, benchOpt_t const * const opt, benchIter_t* const result );
void run_read_ud( const gaspi_rank_t rank, benchData_t* const data );

void read_bd( const gaspi_rank_t rank, const unsigned int total_nmbr_ranks, const unsigned int nmbr_samples, benchOpt_t const * const opt, benchIter_t* const result );
void run_read_bd( const gaspi_rank_t rank, benchData_t* const data );

void wrte_mt_ud( const gaspi_rank_t rank, const unsigned int total_nmbr_ranks, const unsigned int threads_each, const unsigned int nmbr_samples, benchOpt_t const * const opt, benchIter_t* const result );
void run_wrte_mt_ud( const gaspi_rank_t rank, benchData_t* const data );

void read_mt_ud( const gaspi_rank_t rank, const unsigned int total_nmbr_ranks, const unsigned int threads_each, const unsigned int nmbr_samples, benchOpt_t const * const opt, benchIter_t* const result );
void run_read_mt_ud( const gaspi_rank_t rank, benchData_t* const data );


int main( int argc, char *argv[] );


bool power_of( const unsigned int uval, const unsigned int base );

bool multple_of( const unsigned int uval, const unsigned int a );

unsigned int calc_pow( const unsigned int base, const unsigned int exponent );

unsigned int calc_mod( const unsigned int a, const unsigned int b );

double calc_median( double* const samples, const unsigned int nmbr_smpls, const bool preserve );

double calc_average( double const * const samples, const unsigned int nmbr_smpls );


double now(  );


double get_rnd_dval(  );


void prepare_data( const unsigned int threads_each, const gaspi_size_t dlngth_start, const unsigned int nmbr_dlngths, const unsigned int nmbr_samples, const bool averages_only, const bool wrte_read_rnd_dvals, benchData_t* const data );

void reset_results( benchData_t* const data );


void notify_and_wait( const gaspi_segment_id_t segment_id_remote, const gaspi_rank_t rank, const gaspi_notification_id_t notification_id, const gaspi_notification_t notification_value, const gaspi_queue_id_t queue );

//void notify_and_cycle( const gaspi_segment_id_t segment_id_remote, const gaspi_rank_t rank, const gaspi_notification_id_t notification_id, const gaspi_notification_t notification_value )

void write_and_wait( const gaspi_segment_id_t segment_id_local, const gaspi_offset_t offset_local, const gaspi_rank_t rank, const gaspi_segment_id_t segment_id_remote, const gaspi_offset_t offset_remote, const gaspi_size_t size, const gaspi_queue_id_t queue );

void write_notify_and_wait( const gaspi_segment_id_t segment_id_local, const gaspi_offset_t offset_local, const gaspi_rank_t rank, const gaspi_segment_id_t segment_id_remote, const gaspi_offset_t offset_remote, const gaspi_size_t size, const gaspi_notification_id_t notification_id, const gaspi_notification_t notification_value, const gaspi_queue_id_t queue );

//void write_notify_and_cycle( const gaspi_segment_id_t segment_id_local, const gaspi_offset_t offset_local, const gaspi_rank_t rank, const gaspi_segment_id_t segment_id_remote, const gaspi_offset_t offset_remote, const gaspi_size_t size, const gaspi_notification_id_t notification_id, const gaspi_notification_t notification_value )

void read_and_wait( const gaspi_segment_id_t segment_id_local, const gaspi_offset_t offset_local, const gaspi_rank_t rank, const gaspi_segment_id_t segment_id_remote, const gaspi_offset_t offset_remote, const gaspi_size_t size, const gaspi_queue_id_t queue );

void wait_or_die( const gaspi_segment_id_t segment_id, const gaspi_notification_id_t notification_id, const gaspi_notification_t expected );

void wait_for_flush_queue( const gaspi_queue_id_t queue );

//void wait_for_flush_queues(  )

//void wait_if_queue_full( const gaspi_queue_id_t queue, const gaspi_number_t request_size )

void warn_if_queue_full( const gaspi_queue_id_t queue, const gaspi_number_t request_size );

void delete_segment_after_barr( const gaspi_segment_id_t segment_id, const gaspi_group_t group );


#endif