#include "benchmark.h"
#include "prototypes.h"


void notfy_bd( const gaspi_rank_t rank, const unsigned int total_nmbr_ranks, const unsigned int nmbr_samples, benchOpt_t const * const opt, benchIter_t* const result )
{
    if( total_nmbr_ranks != 2 )
    {
        if( !rank ) fprintf( stderr, "Launch w/ exactly two prcs\n" );
        exit( 1 );
    }

    const gaspi_segment_id_t segment = 0;
    SUCCESS_OR_DIE( gaspi_segment_create( segment, (gaspi_size_t)1, GASPI_GROUP_ALL, GASPI_BLOCK, GASPI_ALLOC_DEFAULT ) );

    const gaspi_rank_t target = RIGHT(rank,total_nmbr_ranks);
    const gaspi_rank_t source = LEFT(rank,total_nmbr_ranks);

    const gaspi_notification_id_t data_avl = 0;
    const gaspi_notification_id_t ack = 1;

    const gaspi_queue_id_t queue = 0;


    double *samples = NULL;
    double t0, t1;
    if( rank == 0 )
    {
        if( !opt->alternative_mode )
        {
            samples = (double *)malloc( nmbr_samples*sizeof( double ) );
            NPTR_TEST(samples);

            // Enter global barrier
            SUCCESS_OR_DIE( gaspi_barrier( GASPI_GROUP_ALL, GASPI_BLOCK ) );

            t0 = now(  );
            for( int k = 0; k < nmbr_samples; ++k )
            {
                // Notify rank 1 (data_avl)
                SUCCESS_OR_DIE( gaspi_notify( segment, target, data_avl, (gaspi_notification_t)1, queue, GASPI_BLOCK ) );
                // Wait for notification data_avl
                wait_or_die( segment, data_avl, (gaspi_notification_t)1 );
                // Notify rank 1 (ack)
                SUCCESS_OR_DIE( gaspi_notify( segment, source, ack, (gaspi_notification_t)4, queue, GASPI_BLOCK ) );
                // Wait for notification ack
                wait_or_die( segment, ack, (gaspi_notification_t)4 );
                // Flush single queue used
                wait_for_flush_queue( queue );
                *( samples+k ) = now(  );
            }

            if( nmbr_samples >= 2 )
                for( int k = nmbr_samples-2; k >= 0; --k )
                    samples[k+1] -= samples[k];
                /*
                for( int k = 0; nmbr_samples-2 >= k; ++k )
                    for( int l = k+1; nmbr_samples-1 >= l; ++l )
                        samples[l] -= samples[k];
                */
            *( samples+0 ) -= t0;

            // Calc and store both median and arithmetic mean of samples
            result->t = calc_median( samples, nmbr_samples, false );
            result->t_avg = calc_average( samples, nmbr_samples );
        }
        else
        {
#ifndef NO_WARNING
            warn_if_queue_full( queue, (gaspi_number_t)( 2*nmbr_samples ) );
#endif
            // Enter global barrier
            SUCCESS_OR_DIE( gaspi_barrier( GASPI_GROUP_ALL, GASPI_BLOCK ) );

            t0 = now(  );
            for( int k = 0; k < nmbr_samples; ++k )
            {
                // Notify rank 1 (data_avl)
                SUCCESS_OR_DIE( gaspi_notify( segment, target, data_avl, (gaspi_notification_t)1, queue, GASPI_BLOCK ) );
                // Wait for notification data_avl
                wait_or_die( segment, data_avl, (gaspi_notification_t)1 );
                // Notify rank 1 (ack)
                SUCCESS_OR_DIE( gaspi_notify( segment, source, ack, (gaspi_notification_t)4, queue, GASPI_BLOCK ) );
                // Wait for notification ack
                wait_or_die( segment, ack, (gaspi_notification_t)4 );
            }
            // Flush single queue used
            wait_for_flush_queue( queue );
            t1 = now(  );

            t1 -= t0;
            t1 /= nmbr_samples;

            // Store arithmetic mean t1
            result->t_avg = t1;
        }
    }
    else
    {
        if( !opt->alternative_mode )
        {
            // Enter global barrier
            SUCCESS_OR_DIE( gaspi_barrier( GASPI_GROUP_ALL, GASPI_BLOCK ) );

            for( int k = 0; k < nmbr_samples; ++k )
            {
                // Notify rank 0 (data_avl)
                SUCCESS_OR_DIE( gaspi_notify( segment, target, data_avl, (gaspi_notification_t)1, queue, GASPI_BLOCK ) );
                // Wait for notification data_avl
                wait_or_die( segment, data_avl, (gaspi_notification_t)1 );
                // Notify rank 0 (ack)
                SUCCESS_OR_DIE( gaspi_notify( segment, source, ack, (gaspi_notification_t)4, queue, GASPI_BLOCK ) );
                // Wait for notification ack
                wait_or_die( segment, ack, (gaspi_notification_t)4 );
                // Flush single queue used
                wait_for_flush_queue( queue );
            }
        }
        else
        {
/*
#ifndef NO_WARNING
            warn_if_queue_full( queue, (gaspi_number_t)( 2*nmbr_samples ) );
#endif
*/
            // Enter global barrier
            SUCCESS_OR_DIE( gaspi_barrier( GASPI_GROUP_ALL, GASPI_BLOCK ) );

            for( int k = 0; k < nmbr_samples; ++k )
            {
                // Notify rank 0 (data_avl)
                SUCCESS_OR_DIE( gaspi_notify( segment, target, data_avl, (gaspi_notification_t)1, queue, GASPI_BLOCK ) );
                // Notify rank 0 (ack)
                wait_or_die( segment, data_avl, (gaspi_notification_t)1 );
                // Wait for notification ack
                SUCCESS_OR_DIE( gaspi_notify( segment, source, ack, (gaspi_notification_t)4, queue, GASPI_BLOCK ) );
                // Wait for notification ack
                wait_or_die( segment, ack, (gaspi_notification_t)4 );
            }
            // Flush single queue used
            wait_for_flush_queue( queue );
        }
    }


    free( samples );
    delete_segment_after_barr( segment, GASPI_GROUP_ALL );
    SUCCESS_OR_DIE( gaspi_barrier( GASPI_GROUP_ALL, GASPI_BLOCK ) );
}

void run_notfy_bd( const gaspi_rank_t rank, benchData_t* const data )
{
    benchIter_t *result;
    reset_results( data );

    if( !rank )
        fprintf( stderr, "t[s] t_average[s]\n" );
    
    /*
    for( unsigned int k = 0u; k < data->nmbr_dlngths; ++k )
    {
        result = &data->results[k];
        notfy_bd( rank, data->nmbr_procs, data->nmbr_samples, &data->opt, result );
        if( !rank )
            fprintf( stderr, "%.4e %.4e\n", result->t, result->t_avg );
    }
    */
    result = &data->results[0];
    notfy_bd( rank, data->nmbr_procs, data->nmbr_samples, &data->opt, result );
    if( !rank )
        fprintf( stderr, "%.4e %.4e\n", result->t, result->t_avg );
    
#ifdef DEBUG
    SUCCESS_OR_DIE( gaspi_barrier( GASPI_GROUP_ALL, GASPI_BLOCK ) );
    gaspi_number_t queue_depth;
    SUCCESS_OR_DIE( gaspi_queue_size( (gaspi_queue_id_t)0, &queue_depth ) );
    fprintf( stderr, "DEBUG: Proc %d: queue %d has %d open requests\n", rank, (gaspi_queue_id_t)0, queue_depth );
    SUCCESS_OR_DIE( gaspi_barrier( GASPI_GROUP_ALL, GASPI_BLOCK ) );
#endif
    if( !rank )
        fprintf( stderr, "\n" );
}