#!/bin/bash


echo -n "Building benchmarks.... "
gcc -c -Wall -std=c11 -I<GPI-2 path>/include -L<GPI-2 path>/lib64 *.c -lGPI2 -lpthread -lm -fopenmp
gcc -o bench -I<GPI-2 path>/include -L<GPI-2 path>/lib64 *.o -lGPI2 -lpthread -lm -fopenmp
rm *.o
echo "done."
