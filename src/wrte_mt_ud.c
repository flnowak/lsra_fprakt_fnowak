#include "benchmark.h"
#include "prototypes.h"

#include <omp.h>


void wrte_mt_ud( const gaspi_rank_t rank, const unsigned int total_nmbr_ranks, const unsigned int threads_each, const unsigned int nmbr_samples, benchOpt_t const * const opt, benchIter_t* const result )
{
    if( total_nmbr_ranks != 2 )
    {
        if( !rank ) fprintf( stderr, "Launch w/ exactly two prcs\n" );
        exit( 1 );
    }
    if( !power_of( threads_each, 2u ) )
    {
        if( !rank ) fprintf( stderr, "Launch w/ power-of-2-many threads per proc\n" );
        exit( 1 );
    }
    if( calc_mod( result->d_lngth, threads_each ) )
    {
#ifndef NO_WARNING
        if( !rank ) fprintf( stderr, "WARN: threads_each must divide d_lngth\n" );
#endif
        return;
    }

    const gaspi_segment_id_t segment = 0;
    const gaspi_size_t segment_size = result->d_lngth;
    SUCCESS_OR_DIE( gaspi_segment_create( segment, segment_size, GASPI_GROUP_ALL, GASPI_BLOCK, GASPI_ALLOC_DEFAULT ) );

    gaspi_pointer_t arr;
    SUCCESS_OR_DIE( gaspi_segment_ptr( segment, &arr ) );
    void* const send_arr = arr;

    const gaspi_rank_t target = RIGHT(rank,total_nmbr_ranks);
    const gaspi_rank_t source = LEFT(rank,total_nmbr_ranks);

    const gaspi_notification_id_t data_avl = 0;
    const gaspi_notification_id_t ack = 1;

    const gaspi_queue_id_t queue = 0;

    int threadId, idx;
    gaspi_offset_t off;
    const gaspi_size_t lngth = result->d_lngth/threads_each;


    double *samples = NULL;
    double t0, t1;
    if( rank == 0 )
    {
        // Init local segment w/ random data
        if( opt->wrte_read_rnd_data )
            for( int k = 0; k < result->d_lngth/sizeof( double ); ++k )
                *( (double *)send_arr+k ) = get_rnd_dval(  );
        
        if( !opt->alternative_mode )
        {
            samples = (double *)malloc( nmbr_samples*sizeof( double ) );
            NPTR_TEST(samples);

            omp_set_num_threads( threads_each );
#pragma omp parallel private(threadId,off,idx)
{
            threadId = omp_get_thread_num(  );
            off = threadId*lngth;

#pragma omp master
            SUCCESS_OR_DIE( gaspi_barrier( GASPI_GROUP_ALL, GASPI_BLOCK ) );
#pragma omp barrier

#pragma omp master
            t0 = now(  );
            for( idx = 0; idx < nmbr_samples; ++idx )
            {
                // Write lngth bytes from local to remote segment of rank 1 based on off(set)
                SUCCESS_OR_DIE( gaspi_write( segment, off, target, segment, off, lngth, queue, GASPI_BLOCK ) );
#pragma omp barrier
#pragma omp master
{
                // Notify rank 1 (data_avl)
                SUCCESS_OR_DIE( gaspi_notify( segment, target, data_avl, (gaspi_notification_t)1, queue, GASPI_BLOCK ) );
                // Wait for notification ack
                wait_or_die( segment, ack, (gaspi_notification_t)4 );
                // Flush single queue used
                wait_for_flush_queue( queue );
                *( samples+idx ) = now(  );
}
#pragma omp barrier
            }
}

            if( nmbr_samples >= 2 )
                for( int k = nmbr_samples-2; k >= 0; --k )
                    samples[k+1] -= samples[k];
                /*
                for( int k = 0; nmbr_samples-2 >= k; ++k )
                    for( int l = k+1; nmbr_samples-1 >= l; ++l )
                        samples[l] -= samples[k];
                */
            *( samples+0 ) -= t0;

            // Calc and store both median and arithmetic mean of samples
            result->t = calc_median( samples, nmbr_samples, false );
            result->t_avg = calc_average( samples, nmbr_samples );
        }
        else
        {
#ifndef NO_WARNING
            warn_if_queue_full( queue, (gaspi_number_t)( ( threads_each+1 )*nmbr_samples ) );
#endif
            omp_set_num_threads( threads_each );
#pragma omp parallel private(threadId,off,idx)
{
            threadId = omp_get_thread_num(  );
            off = threadId*lngth;

#pragma omp master
            SUCCESS_OR_DIE( gaspi_barrier( GASPI_GROUP_ALL, GASPI_BLOCK ) );
#pragma omp barrier

#pragma omp master
            t0 = now(  );
            for( idx = 0; idx < nmbr_samples; ++idx )
            {
                // Write lngth bytes from local to remote segment of rank 1 based on off(set)
                SUCCESS_OR_DIE( gaspi_write( segment, off, target, segment, off, lngth, queue, GASPI_BLOCK ) );
#pragma omp barrier
#pragma omp master
{
                // Notify rank 1 (data_avl)
                SUCCESS_OR_DIE( gaspi_notify( segment, target, data_avl, (gaspi_notification_t)1, queue, GASPI_BLOCK ) );
                // Wait for notification ack
                wait_or_die( segment, ack, (gaspi_notification_t)4 );
}
#pragma omp barrier
            }
}
            // Flush single queue used
            wait_for_flush_queue( queue );
            t1 = now(  );

            t1 -= t0;
            t1 /= nmbr_samples;

            // Store arithmetic mean t1
            result->t_avg = t1;
        }
    }
    else
    {
        if( !opt->alternative_mode )
        {
            // Enter global barrier
            SUCCESS_OR_DIE( gaspi_barrier( GASPI_GROUP_ALL, GASPI_BLOCK ) );

            for( int k = 0; k < nmbr_samples; ++k )
            {
                // Wait for notification data_avl
                wait_or_die( segment, data_avl, (gaspi_notification_t)1 );
                // Notify rank 0 (ack)
                SUCCESS_OR_DIE( gaspi_notify( segment, source, ack, (gaspi_notification_t)4, queue, GASPI_BLOCK ) );
                // Flush single queue used
                wait_for_flush_queue( queue );
            }
        }
        else
        {
/*
#ifndef NO_WARNING
            warn_if_queue_full( queue, (gaspi_number_t)( nmbr_samples ) );
#endif
*/
            // Enter global barrier
            SUCCESS_OR_DIE( gaspi_barrier( GASPI_GROUP_ALL, GASPI_BLOCK ) );

            for( int k = 0; k < nmbr_samples; ++k )
            {
                // Wait for notification data_avl
                wait_or_die( segment, data_avl, (gaspi_notification_t)1 );
                // Notify rank 0 (ack)
                SUCCESS_OR_DIE( gaspi_notify( segment, source, ack, (gaspi_notification_t)4, queue, GASPI_BLOCK ) );
            }
            // Flush single queue used
            wait_for_flush_queue( queue );
        }
    }


    free( samples );
    delete_segment_after_barr( segment, GASPI_GROUP_ALL );
    SUCCESS_OR_DIE( gaspi_barrier( GASPI_GROUP_ALL, GASPI_BLOCK ) );
}

void run_wrte_mt_ud( const gaspi_rank_t rank, benchData_t* const data )
{
    benchIter_t *result;
    reset_results( data );

    if( !rank )
        fprintf( stderr, "threads_each data_length[byte] t[s] t_average[s]\n" );
    
    for( unsigned int k = 0u; k < data->nmbr_dlngths; ++k )
    {
        result = &data->results[k];
        wrte_mt_ud( rank, data->nmbr_procs, data->threads_each, data->nmbr_samples, &data->opt, result );
        if( !rank )
            fprintf( stderr, "%u %lu %.4e %.4e\n", data->threads_each, result->d_lngth, result->t, result->t_avg );
    }

#ifdef DEBUG
    SUCCESS_OR_DIE( gaspi_barrier( GASPI_GROUP_ALL, GASPI_BLOCK ) );
    gaspi_number_t queue_depth;
    SUCCESS_OR_DIE( gaspi_queue_size( (gaspi_queue_id_t)0, &queue_depth ) );
    fprintf( stderr, "DEBUG: Proc %d: queue %d has %d open requests\n", rank, (gaspi_queue_id_t)0, queue_depth );
    SUCCESS_OR_DIE( gaspi_barrier( GASPI_GROUP_ALL, GASPI_BLOCK ) );
#endif
    if( !rank )
        fprintf( stderr, "\n" );
}