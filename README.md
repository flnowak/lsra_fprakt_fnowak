# Towards a Comprehensive Set of Benchmarks for the Global Address Space Programming Interface

Yay! You found the repository that contains the code of my student research project. If you haven't read the project `report.pdf` yet, you are welcome to do so now.  
_tl;dr:_ I wrote some microbenchmarks for the Global Address Space Programming Interface ([GASPI](https://www.gaspi.de/#specification)).

## Installation

Use or adapt the build script `src/make.sh`.

#### Requirements

Both a C compiler and a GASPI implementation are needed. For the development of the existing benchmarks

* `gcc` and
* [GPI-2](http://www.gpi-site.com)

were used.

## Usage

Unfortunately, the developed benchmarks lack a true user interface. For each one of them, however, there's a `run_<bench name>` function that iterates the respective test over a `struct benchmarkData`. `src/main.c` demonstrates how these functions are invoked. Note that the binary still needs to be run with `gaspi_run`.

## Author

Florian Alexander Nowak  
B. Sc. Applied Computer Science  
Heidelberg University  
[F.Nowak@stud.uni-heidelberg.de](mailto:F.Nowak@stud.uni-heidelberg.de)

## License

[MIT](https://en.wikipedia.org/wiki/MIT_License)

    Copyright (c) <2019> <Florian Alexander Nowak>
    
    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:
    
    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.
    
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.